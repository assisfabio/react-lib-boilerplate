/// TODO: See https://github.com/storybooks/addon-jsx
/// TODO: "react-boilerplate": "git+https://bitbucket.org/git-zoom/react-boilerplate.git#master"
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Button, Welcome } from '@storybook/react/demo';
import { Foo, Bar } from '../dist/index.es';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ));

storiesOf('Foo', module)
  .add('with text', () => <Foo text="text" />)
  .add('with some emoji', () => (<Foo text='😀 😎 👍 💯' />));

storiesOf('Bar', module)
  .add('with text', () => <Bar text="text" />)
  .add('with some emoji', () => (<Bar text='😀 😎 👍 💯' />));