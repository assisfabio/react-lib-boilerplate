import React, { Component } from 'react'

import { Foo, Bar } from 'zoom-react-lib-boilerplate'

export default class App extends Component {
  render () {
    return (
      <div>
        <h1>Change in Example</h1>
        <Foo text='Foo Props' />
        <Bar text='Bar Props' />
      </div>
    )
  }
}
