import React, { Component } from 'react';
import PropTypes from 'prop-types';

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = "/* add css styles here (optional) */\n\n.Foo_foo__2LY0w {\n  display: block;\n  margin: 2em auto;\n  border: 2px solid #000;\n  font-size: 2em;\n  background: blue;\n}\n";
var styles = { "foo": "Foo_foo__2LY0w" };
styleInject(css);

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();









var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

/**
 * @class Foo
 */

var Foo = function (_Component) {
  inherits(Foo, _Component);

  function Foo() {
    classCallCheck(this, Foo);
    return possibleConstructorReturn(this, (Foo.__proto__ || Object.getPrototypeOf(Foo)).apply(this, arguments));
  }

  createClass(Foo, [{
    key: 'render',
    value: function render() {
      var text = this.props.text;


      return React.createElement(
        'div',
        { className: styles.foo },
        React.createElement(
          'h5',
          null,
          'Change in Foo'
        ),
        React.createElement(
          'p',
          null,
          'Component Foo: ',
          React.createElement(
            'strong',
            null,
            text
          )
        )
      );
    }
  }]);
  return Foo;
}(Component);

Foo.propTypes = {
  text: PropTypes.string
};

var css$1 = "/* add css styles here (optional) */\n\n.Bar_bar__6-tk7 {\n  display: block;\n  margin: 2em auto;\n  border: 2px solid #000;\n  font-size: 2em;\n  background: red;\n}\n";
var styles$1 = { "bar": "Bar_bar__6-tk7" };
styleInject(css$1);

/**
 * @class Bar
 */

var Bar = function (_Component) {
  inherits(Bar, _Component);

  function Bar() {
    classCallCheck(this, Bar);
    return possibleConstructorReturn(this, (Bar.__proto__ || Object.getPrototypeOf(Bar)).apply(this, arguments));
  }

  createClass(Bar, [{
    key: 'render',
    value: function render() {
      var text = this.props.text;


      return React.createElement(
        'div',
        { className: styles$1.bar },
        React.createElement(
          'h5',
          null,
          'Change in Bar Lib'
        ),
        React.createElement(
          'p',
          null,
          'Component Bar modifyed: ',
          React.createElement(
            'strong',
            null,
            text
          )
        )
      );
    }
  }]);
  return Bar;
}(Component);

Bar.propTypes = {
  text: PropTypes.string
};

// alternative, more concise syntax for named exports
// export { default as Foo } from './Foo'

// you can optionally also set a default export for your module
var index = { Foo: Foo, Bar: Bar };

export { Foo, Bar };
export default index;
