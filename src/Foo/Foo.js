/**
 * @class Foo
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './Foo.css'

export default class Foo extends Component {
  static propTypes = {
    text: PropTypes.string
  }

  render() {
    const {
      text
    } = this.props

    return (
      <div className={styles.foo}>
        <h5>Change in Foo</h5>
        <p>Component Foo: <strong>{text}</strong></p>
      </div>
    )
  }
}
