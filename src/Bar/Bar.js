/**
 * @class Bar
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './Bar.css'

export default class Bar extends Component {
  static propTypes = {
    text: PropTypes.string
  }

  render() {
    const {
      text
    } = this.props

    return (
      <div className={styles.bar}>
        <h5>Change in Bar Lib</h5>
        <p>Component Bar modifyed: <strong>{text}</strong></p>
      </div>
    )
  }
}
